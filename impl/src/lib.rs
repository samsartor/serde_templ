#![feature(proc_macro_hygiene)]

use proc_macro::TokenStream as TokenStream1;
use proc_macro2::{Ident, TokenStream};
use quote::{format_ident, quote};
use std::collections::BTreeSet;
use syn::parse_macro_input;

mod syntax;
use syntax::{EofSep, Item, Template};

enum Bound {
    // 0: Fn(1) -> S, S: Serialize,
    SerClosure(Ident, ArgTys),
    // 0: Fn(1) -> bool,
    BoolClosure(Ident, ArgTys),
    // 0: Fn(1) -> I, I: IntoIterator, I::Item: Serialize
    IterSeqClosure(Ident, ArgTys),
    // 0: Fn(1) -> I, K: Serialize, V: Serialize, I: IntoIterator<Item=(K, V)>
    IterMapClosure(Ident, ArgTys),
    // 0: Case<1, Parts>,
    Case(Ident, ArgTys),
    // 0: Fn([init of 1]) -> I, I: IntoIterator<Item=[last of 1]>
    IterArgs(Ident, ArgTys),
}

impl Bound {
    fn to_tokens(
        &self,
        params: &mut Vec<Ident>,
        args: &mut ArgTys,
        clauses: &mut Vec<TokenStream>,
    ) {
        use Bound::*;
        match self {
            SerClosure(t, a) => {
                let ret = format_ident!("S{}", params.len());
                params.push(ret.clone());
                args.append(a);
                let args = a.ty(true);
                clauses.push(quote! { #t: for<'i> Fn(#args) -> #ret });
                clauses.push(quote! { #ret: ::serde::Serialize });
            }
            BoolClosure(t, a) => {
                args.append(a);
                let args = a.ty(true);
                clauses.push(quote! { #t: for<'i> Fn(#args) -> bool });
            }
            IterSeqClosure(t, a) => {
                args.append(a);
                let ret = format_ident!("I{}", params.len());
                params.push(ret.clone());
                let args = a.ty(true);
                clauses.push(quote! { #t: for<'i> Fn(#args) -> #ret });
                clauses.push(quote! { #ret: ::std::iter::IntoIterator });
                clauses
                    .push(quote! { <#ret as ::std::iter::IntoIterator>::Item: ::serde::Serialize });
            }
            IterMapClosure(t, a) => {
                let ret = format_ident!("I{}", params.len());
                params.push(ret.clone());
                let k = format_ident!("S{}", params.len());
                params.push(k.clone());
                let v = format_ident!("S{}", params.len());
                params.push(v.clone());
                args.append(a);
                let args = a.ty(true);
                clauses.push(quote! { #t: for<'i> Fn(#args) -> #ret });
                clauses.push(quote! { #k: ::serde::Serialize });
                clauses.push(quote! { #v: ::serde::Serialize });
                clauses.push(quote! { #ret: ::std::iter::IntoIterator<Item=(#k, #v)> });
            }
            Case(..) => unimplemented!(),
            IterArgs(f, a) => {
                args.append(a);
                let mut init = Vec::new();
                let mut last = None;
                for &i in &a.set {
                    if let Some(l) = last.replace(format_ident!("A{}", i)) {
                        init.push(l);
                    }
                }
                let last = last.unwrap();

                let ret = format_ident!("I{}", params.len());
                params.push(ret.clone());
                clauses.push(quote! { #f: for<'i> Fn((#(&'i #init,)*)) -> #ret });
                clauses.push(quote! { #ret: ::std::iter::IntoIterator<Item=#last> });
            }
        }
    }
}

#[derive(Default)]
struct Case {
    ops: Vec<TokenStream>,
    args: ArgTys,
}

impl Case {
    fn new(args: &Args) -> Self {
        Case {
            args: args.into(),
            ..Default::default()
        }
    }
}

#[derive(Default)]
struct Data {
    cases: Vec<Case>,
    parts: Vec<TokenStream>,
    bounds: Vec<Bound>,
    next_arg: usize,
}

#[derive(Clone, Default)]
struct Args {
    list: Vec<(usize, TokenStream)>,
}

impl Args {
    fn pat(&self) -> TokenStream {
        let list = self.list.iter().map(|(_, p)| p);
        quote! { (#(&#list,)*) }
    }
}

#[derive(Clone, Default)]
struct ArgTys {
    set: BTreeSet<usize>,
}

impl<'a> From<&'a Args> for ArgTys {
    fn from(args: &Args) -> Self {
        // Order must not change
        debug_assert!(args.list.windows(2).all(|w| w[0].0 < w[1].0));

        ArgTys {
            set: args.list.iter().map(|&(i, _)| i).collect(),
        }
    }
}

impl ArgTys {
    fn append(&mut self, args: &ArgTys) {
        self.set.extend(args.set.iter().copied());
    }

    fn ty_params(&self) -> impl Iterator<Item = Ident> + '_ {
        self.set.iter().map(|&i| format_ident!("A{}", i))
    }

    fn ty(&self, hrbt: bool) -> TokenStream {
        let apostrophe = proc_macro2::Punct::new('\'', proc_macro2::Spacing::Joint);
        let lifetime = match hrbt {
            true => format_ident!("i"),
            false => format_ident!("o"),
        };
        let tys = self.ty_params();
        quote! { (#(&#apostrophe #lifetime #tys,)*) }
    }
}

impl Data {
    fn next_part(&mut self, expr: TokenStream, args: &Args) -> (Ident, TokenStream) {
        let i = self.parts.len();
        let args = args.pat();
        self.parts.push(quote! { |#args| #expr });
        let field = format_ident!("p{}", i);
        (format_ident!("P{}", i), quote! { p.#field })
    }

    fn extend_args(&mut self, args: &Args, pat: TokenStream) -> Args {
        let i = self.next_arg;
        self.next_arg += 1;
        let mut list = Vec::with_capacity(args.list.len() + 1);
        list.extend_from_slice(&args.list);
        list.push((i, pat));
        Args { list }
    }

    fn next_case(&mut self) -> (usize, Ident) {
        let id = self.cases.len();
        let ident = format_ident!("Case{}", id);
        self.cases.push(Case::default());
        (id, ident)
    }

    fn case_to_tokens(&self, index: usize) -> TokenStream {
        let ident = &format_ident!("Case{}", index);
        let case = &self.cases[index];
        let args_ty = case.args.ty(false);
        // FIXME: make sure ty params in `args_ty` are included in `params`?
        let (parts_ty, params, wheres) = self.make_bounds();
        let ops = &case.ops;

        quote! {
            struct #ident;

            impl<'o #(,#params)*> ::serde_templ::Case<#args_ty, #parts_ty> for #ident
                where #(#wheres),*
            {
                #[inline(always)]
                fn serialize<R>(r: R, a: #args_ty, p: &#parts_ty) -> Result<R::Ok, R::Error>
                    where R: ::serde::Serializer
                {
                    #(#ops)*
                }
            }
        }
    }

    fn part_tys(&self) -> impl Iterator<Item = Ident> {
        (0..self.parts.len()).map(|i| format_ident!("P{}", i))
    }

    fn make_bounds(&self) -> (TokenStream, Vec<Ident>, Vec<TokenStream>) {
        let mut params = self.part_tys().collect::<Vec<_>>();
        let mut args = ArgTys::default();
        let mut clauses = Vec::new();
        for bound in &self.bounds {
            match bound {
                Bound::Case(..) => continue,
                _ => (),
            }
            bound.to_tokens(&mut params, &mut args, &mut clauses);
        }
        params.extend(args.ty_params());
        (quote! { Parts<#(#params),*> }, params, clauses)
    }
}

fn compile_template<C>(
    template: Template<C>,
    data: &mut Data,
    args: &Args,
    last_arg: Option<Ident>,
) -> TokenStream {
    let count = match template.element_count() {
        Some(n) => quote! { ::std::option::Option::Some(#n) },
        None => quote! { ::std::option::Option::None },
    };
    let aexpr = match last_arg {
        None => quote! { a },
        Some(last) => {
            let init = (0..args.list.len() - 1).map(proc_macro2::Literal::usize_unsuffixed);
            quote! { (#(a.#init,)* &#last,) }
        }
    };
    match template {
        Template::Expr(v) => {
            let (ty, expr) = data.next_part(v.expr, &args);
            data.bounds.push(Bound::SerClosure(ty, args.into()));
            quote! { (&#expr)(#aexpr) }
        }
        Template::Seq { items, .. } => {
            let (index, ident) = data.next_case();
            let mut case = Case::new(args);

            case.ops
                .push(quote! { let mut r = ::serde::Serializer::serialize_seq(r, #count)?; });
            for item in items {
                match item {
                    Item::Single { value, .. } => {
                        let ser = compile_template(value, data, &args, None);
                        case.ops.push(quote! { ::serde::ser::SerializeSeq::serialize_element(&mut r, &#ser)?; });
                    }
                    Item::Splat { iter, .. } => {
                        let (ty, expr) = data.next_part(iter.expr, &args);
                        data.bounds.push(Bound::IterSeqClosure(ty, args.into()));
                        case.ops.push(quote! { for v in (&#expr)(a) {
                            ::serde::ser::SerializeSeq::serialize_element(&mut r, &v)?;
                        } });
                    }
                    Item::CfIf { cond, value, .. } => {
                        let (cond_ty, cond_expr) = data.next_part(cond.expr, &args);
                        data.bounds.push(Bound::BoolClosure(cond_ty, args.into()));
                        let ser = compile_template(value, data, &args, None);
                        case.ops.push(quote! { if (&#cond_expr)(a) {
                            ::serde::ser::SerializeSeq::serialize_element(&mut r, &#ser)?;
                        } });
                    }
                    Item::CfFor {
                        pattern,
                        iter,
                        value,
                        ..
                    } => {
                        let (ty, expr) = data.next_part(iter.expr, &args);
                        let args2 = data.extend_args(args, pattern.expr);
                        data.bounds.push(Bound::IterArgs(ty, (&args2).into()));
                        let ser = compile_template(value, data, &args2, Some(format_ident!("v")));
                        case.ops.push(quote! { for v in (&#expr)(a) {
                            ::serde::ser::SerializeSeq::serialize_element(&mut r, &#ser)?;
                        } });
                    }
                }
            }
            case.ops.push(quote! { ::serde::ser::SerializeSeq::end(r) });

            data.bounds.push(Bound::Case(ident.clone(), args.into()));
            data.cases[index] = case;
            quote! { ::serde_templ::State(#ident, #aexpr, p) }
        }
        Template::Map { items, .. } => {
            let (index, ident) = data.next_case();
            let mut case = Case::new(args);

            case.ops
                .push(quote! { let mut r = ::serde::Serializer::serialize_map(r, #count)?; });
            for item in items {
                match item {
                    Item::Single { key, value, .. } => {
                        let key = compile_template(key.key, data, &args, None);
                        let value = compile_template(value, data, &args, None);
                        case.ops.push(quote! { ::serde::ser::SerializeMap::serialize_entry(&mut r, &#key, &#value)?; });
                    }
                    Item::Splat { iter, .. } => {
                        let (ty, expr) = data.next_part(iter.expr, &args);
                        data.bounds.push(Bound::IterMapClosure(ty, args.into()));
                        case.ops.push(quote! { for (k, v) in (&#expr)(a) {
                            ::serde::ser::SerializeMap::serialize_entry(&mut r, &k, &v)?;
                        } });
                    }
                    Item::CfIf {
                        cond, key, value, ..
                    } => {
                        let (cond_ty, cond_expr) = data.next_part(cond.expr, &args);
                        data.bounds.push(Bound::BoolClosure(cond_ty, args.into()));
                        let key = compile_template(key.key, data, &args, None);
                        let value = compile_template(value, data, &args, None);
                        case.ops.push(quote! { if (&#cond_expr)(a) {
                            ::serde::ser::SerializeMap::serialize_entry(&mut r, &#key, &#value)?;
                        } });
                    }
                    Item::CfFor {
                        pattern,
                        iter,
                        key,
                        value,
                        ..
                    } => {
                        let (ty, expr) = data.next_part(iter.expr, &args);
                        let args2 = data.extend_args(args, pattern.expr);
                        data.bounds.push(Bound::IterArgs(ty, (&args2).into()));
                        let key = compile_template(key.key, data, &args2, Some(format_ident!("v")));
                        let value = compile_template(value, data, &args2, Some(format_ident!("v")));
                        case.ops.push(quote! { for v in (&#expr)(a) {
                            ::serde::ser::SerializeMap::serialize_entry(&mut r, &#key, &#value)?;
                        } });
                    }
                }
            }
            case.ops.push(quote! { ::serde::ser::SerializeMap::end(r) });

            data.bounds.push(Bound::Case(ident.clone(), args.into()));
            data.cases[index] = case;
            quote! { ::serde_templ::State(#ident, #aexpr, p) }
        }
    }
}

#[proc_macro]
pub fn ser(input: TokenStream1) -> TokenStream1 {
    let template = parse_macro_input!(input as Template<EofSep>);

    let (data, ser) = std::panic::catch_unwind(|| {
        let mut data = Data::default();
        let ser = compile_template(template, &mut data, &Args::default(), None);
        (data, ser)
    })
    .unwrap_or((Data::default(), quote! { unimplemented!() }));

    let part_fields = data.part_tys().enumerate().map(|(index, ty)| {
        let field = format_ident!("p{}", index);
        quote! { #field: #ty }
    });
    let parts = data.parts.iter().enumerate().map(|(index, part)| {
        let field = format_ident!("p{}", index);
        quote! { #field: #part }
    });
    let cases = (0..data.cases.len()).map(|i| data.case_to_tokens(i));
    let (parts_ty, params, wheres) = data.make_bounds();

    let output = quote! {
        {
            struct #parts_ty where #(#wheres),* { #(#part_fields),* }

            impl<#(#params),*> ::serde::Serialize for #parts_ty
                where #(#wheres),*
            {
                fn serialize<R>(&self, r: R) -> Result<R::Ok, R::Error>
                    where R: ::serde::Serializer
                {
                    let a = ();
                    let p = self;
                    ::serde::Serialize::serialize(&#ser, r)
                }
            }

            #(#cases)*

            #[allow(unused)]
            let parts = Parts { #(#parts,)* };

            parts
        }
    };

    output.into()
}
