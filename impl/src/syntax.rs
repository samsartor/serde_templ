use proc_macro2::TokenStream;
use std::marker::PhantomData;
use syn::parse::{Nothing, Parse, ParseStream, Result};
use syn::punctuated::Punctuated;
use syn::{token, Token};

/// A separator used to "parse until ...".
pub trait Sep {
    /// How many tokens until the end condition should be checked again? If 0,
    /// parsing should stop.
    fn ends_in(input: ParseStream) -> usize;
}

pub struct EofSep;
impl Sep for EofSep {
    fn ends_in(_: ParseStream) -> usize {
        usize::MAX
    }
}

pub struct ItemSep;
impl Sep for ItemSep {
    fn ends_in(input: ParseStream) -> usize {
        if input.peek(Token![,]) {
            0
        } else {
            1
        }
    }
}

pub struct KvSep;
impl Sep for KvSep {
    fn ends_in(input: ParseStream) -> usize {
        if input.peek(Token![::]) {
            2
        } else if input.peek(Token![:]) {
            0
        } else {
            1
        }
    }
}

pub struct InSep;
impl Sep for InSep {
    fn ends_in(input: ParseStream) -> usize {
        if input.peek(Token![in]) {
            0
        } else {
            1
        }
    }
}

pub struct CfSep;
impl Sep for CfSep {
    fn ends_in(input: ParseStream) -> usize {
        if input.peek(Token![=>]) {
            0
        } else {
            1
        }
    }
}

pub struct Value<S> {
    pub expr: TokenStream,
    separator: PhantomData<S>,
}

impl<S: Sep> Parse for Value<S> {
    fn parse(input: ParseStream) -> Result<Self> {
        use proc_macro2::TokenTree;
        use std::iter::once;

        let mut tokens = TokenStream::new();
        let mut ends_in = 0;
        while !input.is_empty() {
            if ends_in == 0 {
                ends_in = S::ends_in(input)
            };
            if ends_in == 0 {
                break;
            };
            tokens.extend(once(input.parse::<TokenTree>()?));
            ends_in -= 1;
        }

        Ok(Value {
            expr: tokens,
            separator: PhantomData,
        })
    }
}

pub struct MapKey {
    pub key: Template<KvSep>,
    pub colon: Token![:],
}

impl Parse for MapKey {
    fn parse(input: ParseStream) -> Result<Self> {
        Ok(MapKey {
            key: input.parse()?,
            colon: input.parse()?,
        })
    }
}

pub enum Item<K: Parse> {
    Splat {
        splat: Token![..],
        iter: Value<ItemSep>,
    },
    CfFor {
        for_token: Token![for],
        pattern: Value<InSep>,
        in_token: Token![in],
        iter: Value<CfSep>,
        flow_token: Token![=>],
        key: K,
        value: Template<ItemSep>,
    },
    CfIf {
        if_token: Token![if],
        cond: Value<CfSep>,
        flow_token: Token![=>],
        key: K,
        value: Template<ItemSep>,
    },
    Single {
        key: K,
        value: Template<ItemSep>,
    },
}

impl<K: Parse> Item<K> {
    fn fold_count(&self, count: &mut Option<usize>) {
        use Item::*;

        match self {
            Splat { .. } | CfFor { .. } | CfIf { .. } => *count = None,
            Single { .. } => {
                if let Some(count) = count {
                    *count += 1
                }
            }
        }
    }
}

impl<K: Parse> Parse for Item<K> {
    fn parse(input: ParseStream) -> Result<Self> {
        if input.peek(Token![..]) {
            Ok(Item::Splat {
                splat: input.parse()?,
                iter: input.parse()?,
            })
        } else if input.peek(Token![for]) {
            Ok(Item::CfFor {
                for_token: input.parse()?,
                pattern: input.parse()?,
                in_token: input.parse()?,
                iter: input.parse()?,
                flow_token: input.parse()?,
                key: input.parse()?,
                value: input.parse()?,
            })
        } else if input.peek(Token![if]) {
            Ok(Item::CfIf {
                if_token: input.parse()?,
                cond: input.parse()?,
                flow_token: input.parse()?,
                key: input.parse()?,
                value: input.parse()?,
            })
        } else {
            Ok(Item::Single {
                key: input.parse()?,
                value: input.parse()?,
            })
        }
    }
}

pub type SeqItem = Item<Nothing>;
pub type MapItem = Item<MapKey>;

pub enum Template<S> {
    Seq {
        bracket: token::Bracket,
        items: Punctuated<SeqItem, Token![,]>,
    },
    Map {
        brace: token::Brace,
        items: Punctuated<MapItem, Token![,]>,
    },
    Expr(Value<S>),
}

impl<S: Sep> Parse for Template<S> {
    fn parse(input: ParseStream) -> Result<Self> {
        if input.peek(token::Bracket) {
            let content;
            Ok(Template::Seq {
                bracket: syn::bracketed!(content in input),
                items: content.parse_terminated(SeqItem::parse)?,
            })
        } else if input.peek(token::Brace) {
            let content;
            Ok(Template::Map {
                brace: syn::braced!(content in input),
                items: content.parse_terminated(MapItem::parse)?,
            })
        } else {
            Ok(Template::Expr(input.parse()?))
        }
    }
}

impl<S> Template<S> {
    pub fn element_count(&self) -> Option<usize> {
        use Template::*;

        match self {
            Expr(_) => Some(1),
            Seq { items, .. } => {
                let mut count = Some(0);
                for item in items {
                    item.fold_count(&mut count);
                }
                count
            }
            Map { items, .. } => {
                let mut count = Some(0);
                for item in items {
                    item.fold_count(&mut count);
                }
                count
            }
        }
    }
}
