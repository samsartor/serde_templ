#![feature(proc_macro_hygiene)]

use serde_templ::ser;

#[cfg(test)]
macro_rules! assert_ser_eq {
    ($x:tt, $y:expr $(,)?) => {
        let s = ser!($x);
        assert_eq!(::serde_json::to_value(&s).unwrap(), $y);
    };
}

#[test]
fn literals() {
    use serde_json::Value;

    assert_ser_eq!(1, Value::from(1));
    assert_ser_eq!("hello", Value::from("hello"));
    assert_ser_eq!(true, Value::from(true));
    assert_ser_eq!((Option::<()>::None), Value::Null);
}

#[test]
fn exprs() {
    use serde_json::Value;

    let x = 1;
    assert_ser_eq!(x, Value::from(x));
    let x = "\thello\n";
    assert_ser_eq!((x.trim()), Value::from(x.trim()));
    let x = true;
    assert_ser_eq!((!x), Value::from(!x));
}

#[test]
fn seq() {
    use serde_json::json;

    assert_ser_eq!(
        [1 + 0, "hello", !false, Option::<()>::None,],
        json!([1, "hello", true, null,]),
    );
}

#[test]
fn map() {
    use serde_json::json;

    assert_ser_eq!(
        {
            "sum": 1 + 0,
            "hello": "world",
            "facts": !false,
            "missing": Option::<()>::None,
        },
        json!({
            "sum": 1,
            "hello": "world",
            "facts": true,
            "missing": null,
        }),
    );
}

#[test]
fn nested_seq() {
    use serde_json::json;

    assert_ser_eq!(
        [[1 + 0, "world"], ["hello", 2], !false, Option::<()>::None,],
        json!([[1, "world"], ["hello", 2], true, null,]),
    );
}

#[test]
fn nested_map() {
    use serde_json::json;

    let nums = &[1, 0];
    assert_ser_eq!(
        {
            "sum": {
                "value": nums.iter().copied().sum::<i32>(),
                "count": nums.len(),
            },
            "hello": ["world", 2],
            "facts": {
                "are": !false,
                "missing": Option::<()>::None,
            },
        },
        json!({
            "sum": {
                "value": 1,
                "count": 2,
            },
            "hello": ["world", 2],
            "facts": {
                "are": true,
                "missing": null,
            },
        }),
    );
}

#[test]
fn special_keys() {
    use serde_json::json;

    assert_ser_eq!(
        {
            format!("{}{}", "hel", "lo"): "world",
            7: true,
        },
        json!({
            "hello": "world",
            "7": true,
        }),
    );
}

#[test]
fn splatted_seq() {
    use serde_json::json;

    assert_ser_eq!(
        [
            "start",
            ..(0..=2).map(|x| [x, x * 2]),
            ..&["hello", "world"],
            "end",
        ],
        json!(["start", [0, 0], [1, 2], [2, 4], "hello", "world", "end",]),
    );

    assert!(serde_json::to_value(&ser!({
        [0, 1]: true,
    }))
    .is_err());
}

#[test]
fn splatted_map() {
    use serde_json::json;

    assert_ser_eq!(
        {
            "start": true,
            ..(0..=2).map(|x| (x, x * 2)),
            ..["hello", "world"].iter().map(|x| (&x[..1], &x[1..])),
            "end": false,
        },
        json!({
            "start": true,
            "0": 0,
            "1": 2,
            "2": 4,
            "h": "ello",
            "w": "orld",
            "end": false,
        }),
    );
}

#[test]
fn loop_in_seq() {
    use serde_json::json;

    assert_ser_eq!(
        [
            "start",
            for x in 0..=2 => [x, x * 2],
            for x in &["hello", "world"] => x,
            "end",
        ],
        json!([
            "start",
            [0, 0],
            [1, 2],
            [2, 4],
            "hello",
            "world",
            "end",
        ]),
    );

    assert!(serde_json::to_value(&ser!({
        [0, 1]: true,
    }))
    .is_err());
}

#[test]
fn loop_in_map() {
    use serde_json::json;

    assert_ser_eq!(
        {
            "start": true,
            for x in 0..=2 => x: x * 2,
            for x in &["hello", "world"] => &x[..1]: &x[1..],
            "end": false,
        },
        json!({
            "start": true,
            "0": 0,
            "1": 2,
            "2": 4,
            "h": "ello",
            "w": "orld",
            "end": false,
        }),
    );
}

#[test]
fn if_in_simple_seq() {
    use serde_json::json;

    assert_ser_eq!(
        [
            if true => "true",
            if false => "false",
        ],
        json!([
            "true"
        ]),
    );
}

#[test]
fn if_in_simple_map() {
    use serde_json::json;

    assert_ser_eq!(
        {
            if true => "true": 0,
            if false => "false": 0,
        },
        json!({
            "true": 0,
        }),
    );
}

#[test]
fn if_in_seq() {
    use serde_json::json;

    assert_ser_eq!(
        {
            "start": true,
            for x in 0..=3 => x: [
                if x % 2 == 0 => 2,
                if x % 3 == 0 => 3,
            ],
            "end": false,
        },
        json!({
            "start": true,
            "0": [2, 3],
            "1": [],
            "2": [2],
            "3": [3],
            "end": false,
        }),
    );
}

#[test]
fn if_in_map() {
    use serde_json::json;

    assert_ser_eq!(
        {
            "start": true,
            for x in 0..=3 => x: {
                if x % 2 == 0 => 2: true,
                if x % 3 == 0 => 3: true,
            },
            "end": false,
        },
        json!({
            "start": true,
            "0": { "2": true, "3": true },
            "1": {},
            "2": { "2": true },
            "3": { "3": true },
            "end": false,
        }),
    );
}

#[test]
fn multiple() {
    use serde_json::json;

    struct NoClone<T>(T);

    impl<T> Clone for NoClone<T> {
        fn clone(&self) -> Self {
            panic!("cloned")
        }
    }

    impl<T: Iterator> Iterator for NoClone<T> {
        type Item = T::Item;
        fn next(&mut self) -> Option<Self::Item> {
            self.0.next()
        }
    }

    let ser = ser!([..NoClone(0..=2)]);
    assert_eq!(serde_json::to_value(&ser).unwrap(), json!([0, 1, 2]));
    assert_eq!(serde_json::to_value(&ser).unwrap(), json!([0, 1, 2]));
}

#[test]
fn nested_iter() {
    use serde_json::json;

    assert_ser_eq!(
        [
            for ref x in vec![vec![0, 1], vec![1, 2, 3]] => [
                x.clone(),
                for y in x.clone() => format!("{} {}", x.len(), y)
            ],
        ],
        json!([
            [
                [0, 1],
                "2 0",
                "2 1",
            ],
            [
                [1, 2, 3],
                "3 1",
                "3 2",
                "3 3",
            ],
        ]),
    );
}

#[test]
fn tricky_parsing() {
    use serde_json::json;

    mod ns {
        pub const KEY: &'static str = "hello";

        #[allow(non_camel_case_types)]
        pub struct r#in<T>(pub T);
    }

    assert_ser_eq!(
        {
            ns::KEY:"world",
            for ns::r#in(x) in &[ns::r#in("e"), ns::r#in("l")]=>ns::KEY.replace(x,""):x,
            for s in 0..=0 => s: s
        }, json!({
            "hello": "world",
            "hllo": "e",
            "heo": "l",
            "0": 0,
        }),
    );
}
