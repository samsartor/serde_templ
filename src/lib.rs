pub use serde_templ_impl::ser;

#[doc(hidden)]
pub struct State<C, A, P>(pub C, pub A, pub P);

#[doc(hidden)]
pub trait Case<A: Copy, P> {
    fn serialize<R: serde::Serializer>(r: R, args: A, parts: &P) -> Result<R::Ok, R::Error>;
}

impl<'p, C, A: Copy, P> serde::Serialize for State<C, A, &'p P>
where
    C: Case<A, P>,
{
    fn serialize<R: serde::Serializer>(&self, r: R) -> Result<R::Ok, R::Error> {
        C::serialize(r, self.1, self.2)
    }
}
